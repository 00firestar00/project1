package objects;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Class for defining a Course Object.
 * 
 * @author Matthew Fetzer, Evan Jarrett, Dan Larkins
 */
public class Course {
   private static final String SPACE = "  ";
   private static SimpleDateFormat dt = new SimpleDateFormat("h:mm:ss a");

   private String school;
   private String subject;
   private String subjectShort;
   private int number;
   private String title;
   private String days;
   private Date startTime;
   private Date endTime;
   private String instructor;
   private boolean[] weekdays = {false, false, false, false, false, false, false,};

   public Course( String school, String subject, String subjectShort, int number, String title, String days, String startTime, String endTime,
         String instructor ) {
      this.school = school;
      this.subject = subject;
      this.subjectShort = subjectShort;
      this.number = number;
      this.title = title;
      this.days = days;
      this.weekdays = formatDays(days);
      this.instructor = instructor;
      try {
         this.startTime = dt.parse(startTime);
         this.endTime = dt.parse(endTime);
      }
      catch (ParseException e) {
         System.out.println("ysonoob?");
      }
   }

   /**
    * @return the start time
    */
   public Date getStartTime( ) {
      return startTime;
   }

   /**
    * @return the end time
    */
   public Date getEndTime( ) {
      return endTime;
   }

   /**
    * @return the subject
    */
   public String getSubject( ) {
      return subject;
   }

   /**
    * @return the abbreviated subject
    */
   public String getSubjectShort( ) {
      return subjectShort;
   }

   /**
    * @return the course number
    */
   public int getNumber( ) {
      return number;
   }

   /**
    * @return the days of the week
    */
   public String getDays( ) {
      return days;
   }
   
   /**
    * @return the days of the week
    */
   public boolean[] getWeekDays( ) {
      return weekdays;
   }

   /**
    * @return the title
    */
   public String getTitle( ) {
      return title;
   }

   /**
    * @return the school
    */
   public String getSchool( ) {
      return school;
   }

   /**
    * @return the instructor
    */
   public String getInstructor( ) {
      return instructor;
   }

   public String toString( ) {
      StringBuilder sb = new StringBuilder();

      sb.append(getSchool());
      sb.append(SPACE);
      sb.append(getSubjectShort());
      sb.append(SPACE);
      sb.append(getNumber());
      sb.append(SPACE);
      sb.append(getTitle());
      sb.append(SPACE);
      sb.append(getDays());
      sb.append(" - ");
      sb.append(formatTime(getStartTime()));
      sb.append(" - ");
      sb.append(formatTime(getEndTime()));
      sb.append(SPACE);
      sb.append(getInstructor());

      return sb.toString();
   }

   private String formatTime( Date date ) {
      return dt.format(date);
   }

   private boolean[] formatDays( String days ) { 
      boolean[] weekdays = {false, false, false, false, false, false, false,};
      if (days == "Mo") { weekdays[0] = true;}
      if (days == "Tu") { weekdays[1] = true;}
      if (days == "We") { weekdays[2] = true;}
      if (days == "Th") { weekdays[3] = true;}
      if (days == "Fi") { weekdays[4] = true;}
      if (days == "Sa") { weekdays[5] = true;}
      if (days == "Su") { weekdays[6] = true;}
      if (days == "MoWe") { weekdays[0] = true; weekdays[2] = true;}
      if (days == "TuTh") { weekdays[1] = true; weekdays[3] = true;}
      if (days == "ThFr") { weekdays[3] = true; weekdays[4] = true;}
      if (days == "WeFr") { weekdays[2] = true; weekdays[4] = true;}
      if (days == "MoWeFr") { weekdays[0] = true; weekdays[2] = true; weekdays[4] = true;}
      return weekdays;
   }
}
