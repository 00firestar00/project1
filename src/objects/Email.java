package objects;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.sun.mail.smtp.SMTPTransport;

/**
 * @author Matthew Fetzer, Evan Jarrett, Dan Larkins
 */
public class Email {

   private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
   private static final String USER_NAME = "stoof67";
   private static final String PASSWORD = "1243568790";

   /**
    * Send email using GMail SMTP server.
    * 
    * @param recipientEmail TO recipient
    * @param title title of the message
    * @param message message to be sent
    * @throws AddressException if the email address parse failed
    * @throws MessagingException if the connection is dead or not in the connected state or if the message is not a MimeMessage
    */
   public static void Send( String recipientEmail, String title, String message ) throws AddressException, MessagingException {

      // Get a Properties object
      Properties props = System.getProperties();
      props.setProperty("mail.smtps.host", "smtp.gmail.com");
      props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
      props.setProperty("mail.smtp.socketFactory.fallback", "false");
      props.setProperty("mail.smtp.port", "465");
      props.setProperty("mail.smtp.socketFactory.port", "465");
      props.setProperty("mail.smtps.auth", "true");

      props.put("mail.smtps.quitwait", "false");

      Session session = Session.getInstance(props, null);

      // -- Create a new email message --
      final MimeMessage email = new MimeMessage(session);

      // -- Set the FROM and TO fields --
      email.setFrom(new InternetAddress(USER_NAME + "@gmail.com"));
      email.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));
      email.setSubject(title);
      email.setText(message, "utf-8");
      email.setSentDate(new Date());

      SMTPTransport t = ( SMTPTransport ) session.getTransport("smtps");

      t.connect("smtp.gmail.com", USER_NAME, PASSWORD);
      t.sendMessage(email, email.getAllRecipients());
      t.close();
   }
}