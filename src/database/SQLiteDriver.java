package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Wrapper for database connection.
 * Adds timeout functionality and in-use boolean to prevent concurrency issues.
 */
public class SQLiteDriver {

   private Connection connection = null;
   private static final String DB_URL = "jdbc:sqlite:database/classes.db";

   /**
    * Constructor for new mySQLDriver.
    * Initializes a connection to the database upon instantiation.
    * 
    * @throws Exception
    */
   public SQLiteDriver() {
      
      try {
         // load the sqlite-JDBC driver using the current class loader
         Class.forName("org.sqlite.JDBC");
         connection = DriverManager.getConnection(DB_URL);
      }
      catch (SQLException e) {
         e.printStackTrace();
      }
      catch (Exception e) {
         e.printStackTrace();
      }
   }

   /**
    * @return True if the connection is valid, open, and in a usable state. False if unsuitable for use.
    * @throws Exception
    */
   public boolean isValid( ) throws Exception {
      if (connection == null || !connection.isValid(10)) {
         connection = null;
         return false;
      }
      return true;
   }
   
   public void close() throws SQLException {
	   connection.close();
   }

   public PreparedStatement prepareStatement( String query ) throws SQLException {
      return connection.prepareStatement(query);
   }
}
