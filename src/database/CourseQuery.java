package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import objects.Course;

public class CourseQuery {
	
   /**
    * Gets course listings
    */
   public static LinkedList<Course> getCourses( ) {
      SQLiteDriver connection;
      PreparedStatement query;
      LinkedList<Course> logs = new LinkedList<Course>();

      try {
         connection = new SQLiteDriver();
         ResultSet result;

         String prelim_query = "SELECT school, subject, subjectShort, number, title, days, startTime, endTime, instructor FROM courses;";
         query = connection.prepareStatement(prelim_query);
         result = query.executeQuery();
         
         while (result.next()) {
            logs.add(new Course(result.getString(1), //school
                  result.getString(2), //subject
                  result.getString(3), //subjectShort
                  result.getInt(4), //number
                  result.getString(5), //title
                  result.getString(6), //days
                  result.getString(7), //startTime
                  result.getString(8), //endTime
                  result.getString(9))); //instructor
         }

         query.close();
         connection.close();
      }
      catch (SQLException ex) {}

      return logs;
   }
}
