import gui.GUI;

import java.awt.EventQueue;

/**
 * Main driver class for running the GUI
 * 
 * @author Matthew Fetzer, Evan Jarrett, Dan Larkins
 */
public class Driver {

   public static void main( String[] args ) {

      EventQueue.invokeLater(new Runnable() {

         @Override
         public void run( ) {
            try {
               new GUI();
            }
            catch (Exception e) {
               e.printStackTrace();
            }
         }
      });
   }
}
