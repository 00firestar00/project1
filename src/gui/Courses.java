package gui;

import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import objects.Course;

/**
 * @author Matthew Fetzer, Evan Jarrett, Dan Larkins
 */
public class Courses {

   private JPanel container = null;
   private JList classList = null;
   private JScrollPane scrollPane = null;
   private LinkedList<Course> course_list;

   public Courses( LinkedList<Course> course_list ) throws FileNotFoundException {
      this.course_list = course_list;
      this.container = new JPanel();
      this.container.setLayout(new BoxLayout(this.container, BoxLayout.Y_AXIS));

      setList();
      this.container.add(scrollPane);
      this.container.setVisible(true);
   }

   /**
    * Sets the Jlist with the list of courses.
    */
   public void setList( ) {
      this.classList = new JList(this.course_list.toArray());
      this.classList.setMaximumSize(new Dimension(600, 200));
      this.scrollPane = new JScrollPane(this.classList);
   }

   public JPanel getList( ) {
      return container;
   }

   public Object getSelected( ) {
      return this.classList.getSelectedValue();
   }

}
