package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class AdvancedSearch implements ActionListener {
   private JPanel container;
   JButton enterButton;
   private JRadioButton school;
   private JRadioButton department;
   private JRadioButton name;
   ButtonGroup buttons;
   JTextField enter;
   JScrollPane list;
   private JList temp;

   public AdvancedSearch() {
      this.enterButton = new JButton("Enter");
      this.enterButton.addActionListener(this);
      this.buttons = new ButtonGroup();
      this.school = new JRadioButton("By School");
      this.department = new JRadioButton("By Department");
      this.name = new JRadioButton("By Name");
      this.enter = new JTextField();
      this.list = new JScrollPane();
      this.container = new JPanel();
      setLayout(new JList());
   }

   public void setLayout( JList l ) {
      this.temp = l;
      this.container.setLayout(new BoxLayout(this.container, BoxLayout.X_AXIS));
      JPanel panelButtons = new JPanel();
      JPanel enterPanel = new JPanel();
      enterPanel.setLayout(new BoxLayout(enterPanel, BoxLayout.X_AXIS));
      panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.X_AXIS));
      this.buttons.add(this.name);
      this.buttons.add(this.school);
      this.buttons.add(this.department);

      this.name.setSelected(true);
      for (Enumeration<AbstractButton> en = this.buttons.getElements(); en.hasMoreElements();) {
         AbstractButton b = en.nextElement();
         panelButtons.add(b);
      }
      enterPanel.setMaximumSize(new Dimension(200, 50));
      this.list.setMaximumSize(new Dimension(200, 200));
      enterPanel.add(this.enter);
      enterPanel.add(this.enterButton);

      this.container.add(panelButtons);
      this.container.add(enterPanel);
      this.list = new JScrollPane(l);
      this.container.add(this.list);
   }

   public JPanel getPanel( ) {
      return this.container;
   }

   public String getInputSearch( ) {
      return this.enter.getText();
   }

   public void setList( Object[] objects ) {
      this.container.removeAll();
      setLayout(new JList(objects));
   }

   public Object getSelected( ) {
      return this.temp.getSelectedValue();
   }

   @Override
   public void actionPerformed( ActionEvent arg0 ) {
      // TODO Auto-generated method stub

   }

}
