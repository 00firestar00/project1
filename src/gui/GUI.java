package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.Enumeration;
import java.util.LinkedList;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import objects.Course;

import database.CourseQuery;
import database.SearchQuery;

/**
 * Main GUI file for entire application.
 * 
 * @author Matthew Fetzer, Evan Jarrett, Dan Larkins
 */
public class GUI implements ActionListener {
   private AdvancedSearch as;
   private Schedule sched;
   private Courses courses;
   private JFrame mainFrame;
   private JButton goToCourses;
   private JButton goToSearch;
   private JButton goToSchedule;
   private JButton backToMenuButton;
   private JButton addClass;
   private JPanel rightPanel;
   private JPanel mainMenu;
   private JPanel listCantainer;
   private JPanel coursesContainer;
   private JPanel currentPanel;
   private JPanel advancedSearchPanel;
   private JPanel backPanel;
   private JPanel schedPanel;
   private JPanel addPanel;
   private JTextArea info;
   final String INFO_TEXT = "\n\n\n\n\n  Welcome to the class selection menu";
   private LinkedList<Course> log;
   private Boolean isCourses = false;

   //private ArrayList<School> database;

   public GUI() throws FileNotFoundException {
      //this.database = new DatabaseDriver().getSchools();
      this.log = CourseQuery.getCourses();

      this.mainFrame = new JFrame("Menu");
      this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      mainFrame.setSize(new Dimension(600, 400));

      this.mainMenu = new JPanel();
      this.mainMenu.setLayout(new BoxLayout(mainMenu, BoxLayout.X_AXIS));
      this.currentPanel = this.mainMenu;

      this.rightPanel = new JPanel();
      this.rightPanel.setLayout(new BoxLayout(this.rightPanel, BoxLayout.Y_AXIS));

      this.goToCourses = new JButton("Classes");
      this.goToCourses.addActionListener(this);
      goToCourses.setSize(new Dimension(100, 100));
      this.rightPanel.add(goToCourses);

      this.goToSearch = new JButton("Advanced Search");
      this.goToSearch.addActionListener(this);
      goToSearch.setSize(new Dimension(100, 100));
      this.rightPanel.add(this.goToSearch);

      this.goToSchedule = new JButton("View Schedule");
      this.goToSchedule.addActionListener(this);
      this.goToSchedule.setSize(new Dimension(100, 100));
      this.rightPanel.add(this.goToSchedule);

      this.info = new JTextArea();
      this.info.setText(INFO_TEXT);
      this.info.setEditable(false);

      this.mainMenu.add(this.info);
      this.mainMenu.add(this.rightPanel);
      this.mainFrame.add(this.mainMenu);
      this.mainFrame.setVisible(true);

      this.backToMenuButton = new JButton("Back");
      this.backToMenuButton.addActionListener(this);

      this.addPanel = new JPanel();
      this.addPanel.setMaximumSize(new Dimension(600, 50));
      this.addPanel.setLayout(new BoxLayout(this.addPanel, BoxLayout.X_AXIS));
      this.addClass = new JButton("Add Class");
      this.addClass.addActionListener(this);
      this.addPanel.add(this.addClass);
      this.addPanel.add(new JPanel());

      this.backPanel = new JPanel();
      this.backPanel.setMaximumSize(new Dimension(600, 50));
      this.backPanel.setLayout(new BoxLayout(this.backPanel, BoxLayout.X_AXIS));
      this.backPanel.add(this.backToMenuButton);
      this.backPanel.add(new JPanel());

      this.sched = new Schedule();
      this.sched.delete.addActionListener(this);
      this.courses = new Courses(this.log);
      this.as = new AdvancedSearch();

   }

   public void actionPerformed( ActionEvent e ) {

      if (e.getSource() == this.goToCourses) {
         System.out.println("go To Classes button clicked");
         this.isCourses = true;
         if (this.coursesContainer == null)//if courses button has been pushed
         {
            this.coursesContainer = new JPanel();
            this.coursesContainer.setLayout(new BoxLayout(this.coursesContainer, BoxLayout.Y_AXIS));
            this.mainMenu.setVisible(false);
            this.listCantainer = this.courses.getList();
            this.coursesContainer.add(this.listCantainer);
            this.coursesContainer.add(this.addPanel);
            this.coursesContainer.add(this.backPanel);
            this.mainFrame.add(this.coursesContainer);
            this.currentPanel = this.coursesContainer;
            this.currentPanel.setVisible(true);
         }
         else {//if courses has been pushed before.
            this.currentPanel.setVisible(false);
            this.currentPanel = this.coursesContainer;
            this.currentPanel.add(this.addPanel);
            this.currentPanel.add(this.backPanel);
            this.currentPanel.setVisible(true);
         }

      }
      else if (e.getSource() == this.goToSearch) {

         System.out.println("Advanced Button pushed");
         this.isCourses = false;
         this.currentPanel.setVisible(false);

         this.advancedSearchPanel = this.as.getPanel();
         this.advancedSearchPanel.setLayout(new BoxLayout(this.advancedSearchPanel, BoxLayout.Y_AXIS));
         this.as.enterButton.addActionListener(this);
         this.advancedSearchPanel.add(this.addPanel);
         this.mainFrame.add(this.advancedSearchPanel);

         this.advancedSearchPanel.add(this.backPanel);

         this.currentPanel = this.advancedSearchPanel;
         this.currentPanel.setVisible(true);

      }
      else if (e.getSource() == this.as.enterButton) {

         this.advancedSearchPanel.removeAll();
         Boolean check = false;
         System.out.println("enter button on advanced search pressed");
         String buttonSelected = null;
         AbstractButton current;
         String buttonSQLCommand;

         for (Enumeration<AbstractButton> en = this.as.buttons.getElements(); en.hasMoreElements() || !check;) {
            current = en.nextElement();
            if (current.isSelected()) {
               buttonSelected = current.getText();
               check = true;
            }
         }

         if (buttonSelected.compareTo("By School") == 0) {
            buttonSQLCommand = "school";
         }
         else if (buttonSelected.compareTo("By Department") == 0) {
            buttonSQLCommand = "subjectShort";
         }
         else {
            buttonSQLCommand = "name";
         }
         this.currentPanel.setVisible(false);

         this.as.setList(SearchQuery.search(buttonSQLCommand, this.as.getInputSearch()).toArray());

         this.currentPanel = this.as.getPanel();
         this.currentPanel.add(this.addPanel);
         this.currentPanel.add(this.backPanel);
         this.mainFrame.add(this.currentPanel);

         this.currentPanel.setVisible(true);
         this.currentPanel.setLayout(new BoxLayout(this.currentPanel, BoxLayout.Y_AXIS));
         //reload everything
      }
      else if (e.getSource() == this.backToMenuButton) {
         this.currentPanel.setVisible(false);
         this.currentPanel = this.mainMenu;
         this.currentPanel.setVisible(true);
      }
      else if (e.getSource() == this.goToSchedule || e.getSource() == this.sched.delete) {
         this.currentPanel.setVisible(false);
         this.schedPanel = this.sched.getPanel();

         this.currentPanel = this.schedPanel;
         this.currentPanel.add(this.backPanel);
         this.currentPanel.setVisible(true);
         this.mainFrame.add(this.currentPanel);

      }
      else if (e.getSource() == this.addClass) {
         if (this.isCourses) {
            this.sched.addToList((Course) this.courses.getSelected());
         }
         else {
            this.sched.addToList((Course) this.as.getSelected());
         }
      }
   }
}