package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import objects.Course;
import objects.Email;

public class Schedule implements ActionListener {

   private JPanel container;
   private JButton email;
   public JButton delete;
   private JList textList;
   private ArrayList<Course> list;
   private int counter = 0;

   public Schedule() {

      this.container = new JPanel();
      this.email = new JButton("Email");
      this.delete = new JButton("Delete");
      this.delete.addActionListener(this);
      this.email.addActionListener(this);
      this.list = new ArrayList<Course>();
      this.textList = new JList(this.list.toArray());
      this.container.add(this.textList);
      this.container.add(this.email);
      this.container.add(this.delete);

   }

   public void addToList( Course course ) {

      if (!this.list.contains(course)) { //if the courses is already in the schedule
         if (!conflicts(course)) {
            if (counter < 5) { //restricts students to 5 courses
               this.list.add(course);
               this.container.remove(this.textList);
               this.textList = new JList(this.list.toArray());
               this.container.add(this.textList);
               counter++;
            }
            else {
               warning(1);
            }
         }
         else {
            warning(3);
         }
      }
      else {
         warning(2);
      }
   }

   public void deleteCourse( Object object ) {

      this.list.remove(object);
      this.container.remove(this.textList);
      this.textList = new JList(this.list.toArray());
      this.container.add(this.textList);
   }

   public ArrayList<Course> getList( ) {
      return this.list;
   }

   public JPanel getPanel( ) {
      return this.container;
   }

   public String toString( ) {
      String text = "";
      for (Course s : this.list) {
         text += s.toString() + "\n";
      }
      return text;
   }

   @Override
   public void actionPerformed( ActionEvent e ) {
      if (e.getSource() == this.email) {
         try {
            String emailAddress = JOptionPane.showInputDialog(null, "Enter Email Address");
            Email.Send(emailAddress, "Classes", toString());
         }
         catch (NullPointerException e1) {
            System.out.println("no input");
         }
         catch (AddressException e1) {}
         catch (MessagingException e1) {}
      }
      else if (e.getSource() == this.delete) {
         deleteCourse(this.textList.getSelectedValue());
      }
   }

   /**
    * Checks for conflicts between course times
    * 
    * @param new_course
    * @return true if it conflicts with another course
    */
   private boolean conflicts( Course new_course ) {

      for (Course temp : list) {
         for (int i = 0; i < temp.getWeekDays().length; i++) {
            if (temp.getWeekDays()[i] == new_course.getWeekDays()[i]) {
               //if the courses are on the same day
               if (temp.getDays().compareTo(new_course.getDays()) == 0) {
                  if (new_course.getStartTime().compareTo(temp.getStartTime()) == 0) {
                     //if they start at the same time.
                     return true;
                  }
                  if (new_course.getStartTime().compareTo(temp.getEndTime()) < 0 && new_course.getStartTime().compareTo(temp.getStartTime()) > 0) {
                     //if new_class starts while the current class is in session
                     return true;
                  }
                  if (new_course.getEndTime().compareTo(temp.getEndTime()) < 0 && new_course.getEndTime().compareTo(temp.getStartTime()) > 0) {
                     //if new_class ends while the current class is in session
                     return true;
                  }
               }
            }
         }
      }
      return false;
   }

   /**
    * Prints warning message if a conflict occurs
    * 
    * @param n the warning number
    */
   private void warning( int n ) {
      JFrame frame = new JFrame();
      String warning_message = "";
      if (n == 1) {
         warning_message = "You have already selected 5 classes. You must delete one to add this class.";
      }
      else if (n == 2) {
         warning_message = "You have already selected this class.";
      }
      else if (n == 3) {
         warning_message = "The time for this course conflicts with a previous course time. ";
      }
      JOptionPane.showMessageDialog(frame, warning_message, "Warning", JOptionPane.WARNING_MESSAGE);
   }
}
