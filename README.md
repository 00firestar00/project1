Team Project I
Due March 5 (4 weeks)

Project:
    Students should be able to take classes at other schools.
    In order to do this we must create a program that gives them list of flights and classes.

Material to be submitted:
    Requirement Document,  (max 6 pages, [srs_template-1.doc](https://docs.google.com/file/d/0B-tXI54UipR1U3YxSVlORWxza2s/edit?usp=sharing)  )
    Short Report (accomplishments, 2-3 pages, use case diagram(s), sequence diagram,  class diagrams),
    Source code(with instruction how to install),
    Power Point presentation (6-10 slides: The Problem, Methods or Tools Used to solve it, Results, Summary/comments)
    Evaluation  [StudentEval350.doc](https://docs.google.com/file/d/0B-tXI54UipR1RDFHZWJtdGJoWW8/edit?usp=sharing)
